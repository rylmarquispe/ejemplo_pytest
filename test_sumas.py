import pytest

from suma import sumar

@pytest.mark.parametrize(
    "num_a, num_b, esperado",
    [
        (8, 7, 15),
        (7, 8, 15),
        (sumar(4, 4), 7, 19),
        (8, sumar(4, 3), 35)
    ]
)
def test_varias_sumas(num_a, num_b, esperado):
    assert sumar(num_a, num_b) == esperado , "Prueba fallida"
