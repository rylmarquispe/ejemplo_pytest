import pytest
import requests
import json

@pytest.fixture
def api_url():
	return "https://reqres.in/api"

#@pytest.mark.skip
@pytest.mark.parametrize(
    "id_usuario, nombre_usuario",
    [(6,"Tracey"),
    (9,"Tobias")]
    )
def test_usuarios_validos(api_url, id_usuario, nombre_usuario):
    url = api_url + "/users/" + str(id_usuario)
    resp = requests.get(url)
    j = json.loads(resp.text)
    assert resp.status_code == 200, resp.text
    assert j['data']['id'] == id_usuario, resp.text
    assert j['data']['first_name'] == nombre_usuario, resp.text


@pytest.mark.skip
def test_usuarios_invalidos(api_url):
	url = api_url + "/users/100"
	resp = requests.get(url)
	assert resp.status_code == 404, resp.text

